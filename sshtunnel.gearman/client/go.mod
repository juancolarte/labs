module gearman_tunnel_client

go 1.13

require (
	github.com/elliotchance/sshtunnel v1.2.0
	github.com/mikespook/gearman-go v0.0.0-20190528104120-81d00aa9ce73
	golang.org/x/crypto v0.0.0-20191122220453-ac88ee75c92c
)
