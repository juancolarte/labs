package main

import (
    "os"
    "sync"
    "github.com/mikespook/gearman-go/client"
	"fmt"
	"log"
	"time"
	"golang.org/x/crypto/ssh"

	"github.com/elliotchance/sshtunnel"


)

func main() {
    var wg sync.WaitGroup
	tunnel := sshtunnel.NewSSHTunnel(
		// User and host of tunnel server, it will default to port 22
		// if not specified.
		"ubuntu@10.238.69.176",
		// Pick ONE of the following authentication methods:
		//sshtunnel.PrivateKeyFile("path/to/private/key.pem"), // 1. private key
		ssh.Password("password"), // 2. password
		// The destination host and port of the actual server.
		"127.0.0.1:4730",
		// The local port you want to bind the remote port to.
		// Specifying "0" will lead to a random port.
		"0",
	)
	tunnel.Log = log.New(os.Stdout, "", log.Ldate|log.Lmicroseconds)
	go tunnel.Start()
	time.Sleep(100 * time.Millisecond)
	// Tunnel must be ready now do the request now.
	//url := fmt.Sprintf("http://localhost:%d", tunnel.Local.Port)

    cn_string := fmt.Sprintf("127.0.0.1:%d", tunnel.Local.Port)
	c, err := client.New(client.Network, cn_string)
    if err != nil {
        log.Fatalln(err)
    }
    defer c.Close()
	c.ErrorHandler = func(e error) {
		log.Println(e)
		os.Exit(1)
	}
    echo := []byte("Some\x00 Stuff")
    wg.Add(1)
    echomsg, err := c.Echo(echo)
    if err != nil {
        log.Fatalln(err)
    }
    log.Println(string(echomsg))
    wg.Done()
    jobHandler := func(job *client.Response) {
        log.Printf("%s", job.Data)
        wg.Done()
    }
    handle, err:= c.Do("reverse", echo, client.JobNormal, jobHandler)
	if err != nil {
		log.Fatalln(err)
	}
    wg.Add(1)
    status, err := c.Status(handle)
    if err != nil {
        log.Fatalln(err)
    }
    log.Printf("%t", status)
    wg.Wait()
}
