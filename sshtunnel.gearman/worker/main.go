package main

import (
	"fmt"
	"log"
	"net"
	"os"
	"time"

	"github.com/elliotchance/sshtunnel"
	"github.com/mikespook/gearman-go/worker"
	"github.com/mikespook/golib/signal"
	"golang.org/x/crypto/ssh"
)

func Reverse(job worker.Job) ([]byte, error) {
	log.Printf("Reverse: Handle=[%s]; UID=[%s], Data=[%s]\n",
		job.Handle, job.UniqueId, string(job.Data()))
	runes := []rune(string(job.Data()))
	for i, j := 0, len(runes)-1; i < j; i, j = i+1, j-1 {
		runes[i], runes[j] = runes[j], runes[i]
	}
	data := []byte(string(runes))
	return data, nil
}

func main() {
	log.Println("Starting ...")
	defer log.Println("Shutdown complete!")
	tunnel := sshtunnel.NewSSHTunnel(
		// User and host of tunnel server, it will default to port 22
		// if not specified.
		"ubuntu@10.238.69.176",
		// Pick ONE of the following authentication methods:
		//sshtunnel.PrivateKeyFile("path/to/private/key.pem"), // 1. private key
		ssh.Password("password"), // 2. password
		// The destination host and port of the actual server.
		"127.0.0.1:4730",
		// The local port you want to bind the remote port to.
		// Specifying "0" will lead to a random port.
		"0",
	)
	tunnel.Log = log.New(os.Stdout, "", log.Ldate|log.Lmicroseconds)
	go tunnel.Start()
	time.Sleep(100 * time.Millisecond)
	// Tunnel must be ready now do the request now.
	//url := fmt.Sprintf("http://localhost:%d", tunnel.Local.Port)
	w := worker.New(worker.Unlimited)
	defer w.Close()
	w.ErrorHandler = func(e error) {
		log.Println(e)
		if opErr, ok := e.(*net.OpError); ok {
			if !opErr.Temporary() {
				proc, err := os.FindProcess(os.Getpid())
				if err != nil {
					log.Println(err)
				}
				if err := proc.Signal(os.Interrupt); err != nil {
					log.Println(err)
				}
			}
		}
	}
	w.JobHandler = func(job worker.Job) error {
		log.Printf("Data=%s\n", job.Data())
		return nil
	}
	cnn_string := fmt.Sprintf("127.0.0.1:%d", tunnel.Local.Port)
	w.AddServer("tcp4", cnn_string)
	w.AddFunc("reverse", Reverse, worker.Unlimited)
	go w.Work()
	//sh := signal.NewHandler()
	//sh.Bind(os.Interrupt, func() bool {return true})
	//sh.Loop()
	signal.Bind(os.Interrupt, func() uint { return signal.BreakExit })
	signal.Wait()

}
