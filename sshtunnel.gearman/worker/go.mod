module gearman_tunnel_worker

go 1.13

require (
	github.com/elliotchance/sshtunnel v1.2.0
	github.com/mikespook/gearman-go v0.0.0-20190528104120-81d00aa9ce73
	github.com/mikespook/golib v0.0.0-20151119134446-38fe6917d34b
	golang.org/x/crypto v0.0.0-20201117144127-c1f2f97bffc9
	gopkg.in/mgo.v2 v2.0.0-20190816093944-a6b53ec6cb22 // indirect
)
