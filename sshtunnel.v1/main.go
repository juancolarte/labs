package main

import (
	"fmt"
	"log"
	"os"
	"time"

	"github.com/elliotchance/sshtunnel"
	"github.com/go-resty/resty/v2"
	"golang.org/x/crypto/ssh"
)

func main() {
	tunnel := sshtunnel.NewSSHTunnel(
		// User and host of tunnel server, it will default to port 22
		// if not specified.
		"ubuntu@10.238.69.176",
		// Pick ONE of the following authentication methods:
		//sshtunnel.PrivateKeyFile("path/to/private/key.pem"), // 1. private key
		ssh.Password("pass1234"), // 2. password
		// The destination host and port of the actual server.
		"127.0.0.1:4730",
		// The local port you want to bind the remote port to.
		// Specifying "0" will lead to a random port.
		"0",
	)
	tunnel.Log = log.New(os.Stdout, "", log.Ldate|log.Lmicroseconds)
	go tunnel.Start()
	time.Sleep(100 * time.Millisecond)
	// Tunnel must be ready now do the request now. 
	url := fmt.Sprintf("http://localhost:%d", tunnel.Local.Port)
	client := resty.New()
	resp, err := client.R().EnableTrace().Get(url)
	if err != nil {
		fmt.Println("\n:", "error")
	} else {
		fmt.Println(" RESP: \n:", resp)
	}


}
